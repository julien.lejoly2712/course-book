Le diagramme d'activité sert à donner une vision globale et temporelle d'une partie dynamique d'un système. C'est a dire un enchainement d'activité que l'utilisateur ne voit pas forcément et qui sont opérée par un système.


Cela peut servir à modéliser un algorithme, la dynamique d'un cas d'utilisation, ou un "processus métier".


## Fonctionnement du diagramme

![Distributeur de billets](https://www.nawouak.net/exercises/uml/images/activity--01.gif)


Le diagramme est divisé en 2 colonnes (qui sont appellées swimlanes), la colonne de gauche sert à représenter les intéraction avec le système. Tandis que la colonne de droite est la dynamique du système en elle même. Cela permet de modéliser les actions entre le systèmes et les différents acteurs.

Le début et la fin sont symbolisés par un point noir ou un point noir entouré. Pour symboliser la fin d'un seul flux seulement on utilise un rond avec une croix dedans.

Il y a ensuite différents "noeuds" sur notre diagramme :

| Symbole | Nom | Description |
| ---- | ---- | --- |
| Ovale | Noeud d'exécution | Fait une action |
| Losange (1 input, plusieurs output) | Noeud de décision | En fonction d'une condition, elle va continuer dans un ou l'autre direction. Ses conditions sont représentés sur les lignes |
| Losange (plusieurs input, 1 output) | Noeud de fusion | A l'inverse d'un noeud de décision qui permet de diviser le processus en plusieurs possibilités, le noeud de fusion va recombiner les différentes possibilités. | 
| Barre (1 input, plusieurs output) | Fork | Cela permet de créer des flux parallèles (des actions qui vont se déclencher simultanément) |
| Barre (plusieurs input, 1 output) | Join | Fait l'inverse de fork en recombinant les flux parallèles en un seul |
| Un genre de sablier | Evenement temporel | Modélise un évènement qui se déclenche à un moment prédéfinis (par exemple fin du mois) |
| Un rectangle avec une flèche et avec une "réception de flèche" | Envoi et réception d'un signal | Symbolise une signal asynchrone. C'est a dire que le programme va envoyer un signal vers un autre thread et attendre qu'une tache soit effectué avant de continuer |

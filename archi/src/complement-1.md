### Complément à 1
```
74 en décimal   : 01001010 
-74 en comp à 1 : 10110101 (c'est l'inversion de tout les bits)
```

Pour faciliter les opérations arithmétiques nous pouvons utiliser une autre méthode. Celle d'inverser tous les bits.

Donc pour représenter $ 74_{10} $ sur 8 bits en complément à 1, on obtient `01001010` et pour changer le signe, on inverse tous les bits ce qui donne donc `10110101`. Le bit de signe est donc toujours en action.




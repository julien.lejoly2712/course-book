### Complément à 2

```
74                    : 01001010
-74 en complément à 1 : 10110101 (inversion de tous les bits)
-74 en complément à 2 : 10110110 (complément à 1 + 1)
```

Le complément à 2 corresponds au complément à 1 vu précédemment + 1. Mais il peut être calculé plus rapidement simplement en inversant tout à partir du premier 1 en partant de la gauche:

```
74                    : 01001010
-74 en complément à 2 : 10110110 (on inverse inverse tout jusqu'au premier 1 de gauche)
```

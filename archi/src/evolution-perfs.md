## L'évolution des performances 
A partir des années 70s, avec l'intégration toujours plus poussée des composants sur une puce et l'augmentation de la capacité de calcul

En 1965 Gordon Moore, constate que depuis 1959, la complexité des circuits intégrés à base de transistors double tous les ans à coût constant. Et il postule que cela va continuer ainsi; c'est la première loi de Moore.

En 1975, il revoit sa loi, il précisa sa loi en disant que le nombre de transitors sur une puce de silicium (microprocesseurs, mémoires) doublera tous les deux ans. C'est la deuxième loi de Moore. Qui s'est avérée être assez proche de la réalité jusqu'a 2010. 


## Histoire de l'informatique
Dès le début de l'humanité, nous avons commencé à compter. D'abord on a compté en **base 1** (base unitaire). Par exemple, 1 mouton = 1 caillou (d'où *calculus* qui donne le mot *calcul* en français)

> Un *nombre* est une quantité que l'on veut représenter, tandis qu'un *chiffre* est un symbole qui va être utiliser pour représenter le nombre.

Ensuite on a commencé à utiliser la *numérotation de position*. C'est toujours ce que l'on utilise aujourd'hui, c'est à dire que la position d'un chiffre dans l'écriture d'un nombre a de l'importance. 

Par exemple, en **base 10** (ce que l'on utilise au quotidien pour compter), si je prends le nombre "542", on peut le décomposer comme ceci :

$$
542 = 5\*10^2 + 4\*10^1 + 2\*10^0
$$


C'est ainsi que le *zéro* fait son apparition, avant représenter "rien" n'avait pas d'utilité, mais en numérotation de position, ça a une utilité très claire d'éviter la confusion.

$$
1 \  1 \neq 11 \neq 101 \neq 1001 \neq 1010 
$$

Voici quelques exemples de bases (celle indiquées en gras sont très liées à l'informatique)


| Base | Nom | Usage | Origine |
| --- | --- | --- | --- |
| 1 | Unitaire | Comptage (doigts, cailloux, entailles, etc) |  |
| **2** | **Binaire** | **Logique, électronique, informatique** |  |
| 5 | Quinaire | | Aztèques (doigts d'une main) |
| 7 | Septénaire | Jours de la semaine, notes (tons) | | 
| **8** | **Octal** | **Informatique** | **Premiers ordinateurs** |
| 10 | Décimal | Système le plus répandu | Chinois (doigts des 2 mains) | 
| 12 | [Duodécimal](https://www.youtube.com/watch?v=U6xJfP7-HCc) | Mois, heures, musique (ton et demi-tons) | Egyptiens |
| **16** | **Hexadécimal** | **Informatique** | | 
| 20 | Vicésimal |  | Mayas (doigts + orteils) |
| 60 | sexagésimal | Trigonométrie (angles), minutes, secondes | Babyloniens, indiens, arabes, ... |


### Sortir de cette impasse
* Changement radical de technologie
    * Remplacement du Silicium par de l'arséniure de gallium
    * Supraconducteurs (mais nécessite une température *extrènement froide*)
    * Composants optiques
    * Dispositifs quantiques
* A court terme
    * Multiprocesseurs (plusieurs coeurs et processeurs interconnectés)
    * Fermes de serveur (architectures en parallèles distribuées)
    * Programmation parallèle efficace


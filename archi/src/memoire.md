# La hierarchie des mémoires
La capacité est croissante en dessendant et le prix et la vitesse est croissant en montant

1. Registres (SRAM)
2. Mémoire cache 
3. Mémoire centrale (DRAM)
4. Mémoire de masse primaire
5. Mémoire de masse secondaire

## SRAM vs DRAM
La SRAM est constituée de bascules, elle est donc beaucoup plus rapide mais aussi beaucoup plus cher. 

En revanche la DRAM fonctionne avec des condensateurs et transistors, il faut donc les recharcher pour les empécher de se décharger tout seul.

## FLASH
* RWM (Read-Write Memory) : on parle de mémoire vive (par abus de language on  dit aussi RAM)
* ROM (Read-Only Memory) : on parle de mémoire morte (le contenu est défini par le fabricant)
* PROM (Programmable ROM) : Mémoire morte programmable une fois par l'utilisateur
* EPROM (Erasable Programmable ROM) : Mémoire morte reprogrammable un certain nombre de fois (avec des ultraviolents pendant 30 minutes)
* EEPROM (Electrically Erasable Programmable ROM) : effacement plus rapide et sélectif (1 minute)
* FLASH : forme améliorée de EEPROM utilisée par les USB, SSD, SD, etc qui permet d'avoir une meilleure performance en lecture et écriture

## Near line ?
## Séquentiel vs semi-séquentiel vs direct
Séquentiel : il faut tout lire pour arriver au secteur qui nous interesse
Semi-séquentiel : on peut déjà se positionner sur la bonne piste, mais il faut lire toute la piste
Direct : on peut accéder directement à l'information
Accès par le contenu : 

## PATA et SATA
Cable pour le transfer de données : Parallel vs série

## Vocabulaire important
Seek time = temps pour changer de piste 
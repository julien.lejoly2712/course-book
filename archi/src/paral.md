# Paralélismes
## Types de paralélisme
* Interne : plusieurs tache en paralèle au sein d'un même processeur (pipelining, processeur vectoriel, etc)
* Externe : plusieurs cores (plusieurs processeurs) (par exemple, multi-core, cluster, etc)

* Transparent : l'OS s'occupe de faire exécuter les processus en paralèle (l'utilisateur n'a pas à gérer)
* Explicite : Quand l'utilisateur doit concevoir son programme de manière explicitement paralèle

## Systèmes parallèles

|  | Une instruction | Plusieurs instructions | 
| --- | --- | --- | 
| Un flux de données | SISD (le classique de Von Neumann, paralèle avec les pipelines) | MISD (n'existe pas en pratique) | 
| Plusieurs flux de données | SIMD | MIMD |


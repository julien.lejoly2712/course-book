## Présentation générale d'un ordinateur
Un ordinateur est une machine automatique de traitement de l'information obéissant à des programmes (suites d'instructions arithmétiques et logiques). Il y a donc la partie matérielle et la partie logicielle.

* L'**unité centrale** (processeur, mémoire centrale, gestionaire d'entrée sorties)
* Les **périphériques d'entrée** permettent d'acquérir des informations (par clavier, souris, scanner, etc)
* Les **périphériques de sortie** permettent de restituer des informations (par clavier, souris, scanner, etc)
* Des **périphériques de stockage** (mémoire secondaire) internes ou externes (ex. disque dur, lecteur-graveur CD, clé USB, etc)
* Les **périphériques de communication** soit les cartes réseau (éthernet, bluetooth, wifi) ou les modems (téléphonique, ADSL, RNIS)

Il y a différentes gammes d'ordinateurs (allant du moins cher au plus cher et évolué)

* Les **ordinateurs jetables** qui sont des puces très simples et coutant moins d'un euro produites en très grandes série pour un usage précis (exemple, carte musicale, puce RFID)
* Les **ordinateurs embarqués** est un ordinateur inclu dans un autre appareil dans le but de le piloter (électroménager, MP3, TV, scanner, pace maker, jeux, armement, distributeurs, etc). Ces ordinateurs coûtent en général quelques EUR.
* Les **téléphones intelligents et consoles de jeu** est une catégorie qui se rapproche beaucoup de l'ordinateur classique maus qui peut être réserver à des usages plus précis et à être moins évolutif dans ses performances et gamme de prix.
* Les **ordinateurs personnels**, soit les PC, en général utilisé par un utilisateur à la fois, la gamme de prix varie plus (ex tours, portables, tablettes)
* Les **serveurs** est un ordinateur prévu pour servir des services à plusieurs utilisateurs (stockage de fichier, calcul, serveur Web, etc). En général on entend par là des ordinateurs plus puissant que des ordinateurs personnels, mais il peu aussi il y avoir des "miniserveurs" qui soit tout autant puissant ou moins puissant (ancien ordinateur portable, raspberry pi, etc)
* Les **cluster** (ou **ferme de serveurs** ou **data center**) sont prévu pour fournir une puissance de calcul et/ou une grande capacité de stockage en connectant plusieurs serveurs entre eux pour se répartir les taches.
* Les **mainframes** (ou **super-ordinateurs**) avais des architectures très spécifiques, prévu pour un usage très particulier avec une puissance de calcul très grande (qui sont maintenant remplacé par les data centers qui sont plus performant et coute moins cher). En revanche il existe encore des mainframes, principalement dans le secteur bancaire ou une migration serait trop couteuse.

Le **cloud** n'est rien d'autre qu'un ou plusieurs serveurs ou fermes de serveurs qui fournissent des services à travers internet. L'interet est de mutualiser des ressources, rendre l'information accessible de n'importe où et de synchroniser les données à un moindre coût.

Il existe différents types de *services cloud* :

* Le **IaaS** (Infrastructure as a Service) qui comprends le réseau, le serveur + stockage et la virtualisation (souvent ce ne sont pas des ordinateurs dédiés mais des machines virtuelles tournant sur de gros ordinateurs)
* Le **PaaS** (Platform as a Service) qui comprends réseau, serveur + stockage, virtualisation mais aussi le système d'exploitation et les logiciels de bases (par exemple, serveur web)
* Le **SaaS** (Software as a Service) qui comprends réseau, serveur + stockage, virtualisation, système d'exploitation, logiciel de base, et logiciel cible (par exemple webapp) déjà installé pret à être utilisé. 

Maintenant pour comprendre un peu plus dans les détails comment un ordinateur monoprocesseur fonctionne, comme vu précédemment avec l'architecture de Von Neumann nous avons :

* Une unité centrale (CPU) avec une unité de controle, une unité arithmétique et logique (ALU) et un système de registre (mémoires rapides pour l'ALU)

Mais maintenant il y a aussi le principe de bus de communication (introduit par le PDP-8) qui permet d'interconnecter les composants de l'ordinateurs. 


# Le système RAID
Les disques dur de grande capacité étant très cher. Le système RAID avait à la base été conçu pour créer un « gros disque » (appellé *grappe*), sur base de disque plus petit.

RAID signifie *Redundant Array of Inexpensive Disks*, puis qui fut plus tard renomé en *Redundant Array of Independent Disks*. Car avec l'évolution des technologie, le but des système RAID a changé.

## RAID 0
![RAID 0](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/RAID_0.svg/800px-RAID_0.svg.png)

Le but de RAID 0 est de grouper plusieurs disques physique pour créer un volume de plus grande capacité. Cela permet aussi d'avoir des meilleurs performances, en **écriture**, en écrivant sur plusieurs disques en parallèle.

On peut donc aussi avoir un niveau de **lecture** en lisant des morceaux simultanément.

Le niveau RAID 0 n'offre *aucune* tolérence aux pannes, au contraire, il diminue la fiabilité globale car il suffit qu'un seul disque tombe en panne pour que toutes les données soient perdues.

## RAID 1
![RAID 1](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/RAID_1.svg/800px-RAID_1.svg.png)

Le but de RAID 1 est d'offrir une tolérance aux pannes en dupliquant les données de chaque disque sur un autre disque (ainsi il y a toujours un nombre pair de disques).

Ce système permet aussi une meilleur performance en **lecture** en pouvait lire simultanément la même donnée sur plusieurs disques.

## RAID 2
![RAID 2](https://upload.wikimedia.org/wikipedia/commons/4/4b/Raid_2.png)

Ce niveau n'est plus utilisé. il utilisait le code de Hamming pour protéger les données contre la perte d'un disque. Chaque bit d'un octet est stoqué sur un disque différent.

## RAID 3
![RAID 3](https://upload.wikimedia.org/wikipedia/commons/f/f9/RAID_3.svg)

Ce niveau est une version simplifiée du niveau 2. Il fonctionne en ajoutant simplement 1 bit de parité (et donc un disque). Ainsi quand un disque tombe en panne et est remplacé on peut "réparer" les données grâce au bit de parité.

## RAID 4
![RAID 4](https://upload.wikimedia.org/wikipedia/commons/7/7c/RAID_4-ru.svg)

Les données sont écrites bloc par bloc de manière circulaire sur N disques (et non plus bit par bit comme dans les 2 niveaux précédents)


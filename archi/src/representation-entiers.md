# Représentation des nombres entiers
## BCD (Binary Coded Decimal)
Pour faciliter la vie du programmeur (quand l'assembleur n'était pas encore très utilisé et que les données étaient manuellement codées en binaire) le système "BCD" (Binary Coded Decimal) a été créé.

A la place de coder un nombre en binaire directement : 

```
14 (decimal) = 1110 (binaire) 
```

On va le codé par équivalent décimal (soit on converti chaque chiffre décimal en nombre binaire) :

```
14 (decimal) = 0001 0100 (bcd) 
```

Ce système, bien que plus facile pour le programmeur est plus complexe pour le processeur, donc avec l'arrivée de l'assembleur et autres, ce système est vite abandonné.

<!-- Addition en BCD -->


## Oracle SQL
SQL veut dire Structured Query Language et permet d'intéragir avec les bases de données. Les SGBD étant différents, les languages SQL varie aussi même si certaines choses sont standardisées. 

SQL est divisé en 4 parties

* DDL (Data Definition Language) qui permet de gérer le schéma d'une base de donnée 
* DML (Data Manipulation Language) qui permet de gérer les données en elle même (les ajouter, supprimer, modifier et lire)
* DCL (Data Control Language) qui permet de grérer les permissions au données pour chaque utilisateur SGBD. Ces utilisateurs ne correspondent pas aux utilisateurs finaux mais bien aux utilisateurs du SGBD (donc seulement sur le serveur)

> **Attention**: Tout ce chapitre est consacré à Oracle SQL. Et beaucoup de choses peuvent différer beaucoup, voir ne pas exister dans d'autres SGBD.
# Gérer les tables

Dans cet exemple on crée une table "Persons" qui a les colonnes (attributs) PersonID, LastName, FirstName, Address et City qui ont toutes des types particuliers tel que `varchar(255)` qui indique une chaine de caractère de taille variable et de maximum 255 caractères, et `int` pour un entier.

```sql
 CREATE TABLE Persons (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255)
);
```

En utilisant `ALTER TABLE` on peut modifier les informations d'une table que l'on a déjà créé. Dans cet exemple on ajoute un contrainte `PRIMARY KEY` pour la colonne `PersonID`. Ceci est la même chose que d'ajouter `PRIMARY KEY (ID)` à la fin de la requète de création de table.

```sql
ALTER TABLE Persons ADD PRIMARY KEY (PersonID);
```

Imaginons que nous n'avons plus besoin de cette table, on peut alors la supprimer avec `DROP`.

```sql
DROP TABLE Persons;
```


# Gérer des tuples dans une table
Dans cet exemple on ajoute une nouvelle ligne (tuple) dans notre table, on précise d'abord la liste des colonnes auquel on va ajouter un résultat, puis on ajoute les valeurs correspondantes.

```sql
INSERT INTO Persons (PersonID, LastName, FirstName, Address, City) VALUES (1, 'Shumi', 'Roger', 'Rue du moulin, 13', 'Liège');
```

Les deux lignes décrites ci-dessus ont le même effet. Elles sélectionnent tous les tuples qui corespondent à la condition "PersonID=1" de la table "Persons" avec toutes les propriétés (c'est ce que veut aussi dire le `*`, c'est un "wildcard" qui inclut toutes les propriétés).

```sql
SELECT (PersonID, LastName, FirstName, Address, City) FROM Persons WHERE PersonID=1;
SELECT * FROM Persons WHERE PersonID=1;
```

Dans cet exemple on change les attributs Address et City dans tous les tuples qui remplissent la condition `PersonsID=1` dans la table "Persons". A noter que quand on a une chaine de caractères qui contient un `'` on peut quand même l'inclure en doublant le `'`.

```sql
UPDATE Persons SET Address='Rue de l''église', City='Verviers' WHERE PersonsID=1;
```

Mais on peut aussi modifier une table en se basant sur les valeurs de celle ci. 

```sql
-- Ceci est un exemple dans une autre table où l'on modifie le montant en l'augmentant de 10% 
UPDATE Bien SET montant = montant * 1.1 WHERE id_bien = 4;
```

On supprime Roger en supprimant tous les éléments de notre table "Persons" où `PersonsID=1`.

```sql
DELETE FROM Persons WHERE PersonsID=1;
```

## Des SELECT plus avancés
Maintenant pour avoir un meilleur formatage, on peut utiliser des clauses `SELECT` plus évoluées.

Voici un exemple avec une date que l'on formatte en texte (sous le format tel que '12/12/2022') :

```sql
SELECT TO_CHAR(date_offre, 'dd/mm/yy')
FROM Offre
```

Renomer le nom d'une colonne avec `AS`

```sql
SELECT TO_CHAR(date_offre, 'dd/mm/yy') AS date
FROM Offre
```

Concaténer des colonnes et des chaines de caractères

```sql
SELECT 'La date est ' || date_offre || ' et le statut est ' || statut AS ma_super_colonne_inutile
FROM Offre
```

## Des WHERE plus avancés
On peut faire différentes opération de base dans les clauses `WHERE` tel que >, <, <=, >=, =

```sql
SELECT * FROM Offre WHERE id_offre = 4
```

On peut ensuite lier plusieurs comparaisons avec des opérateurs booléens (AND, OR, NOT)

```sql
SELECT * FROM Offre WHERE NOT id_offre = 4 AND montant > 1000;
```

Pour vérifier si il n'y a aucune valeur, on peut utiliser `IS NULL`

```sql
SELECT * FROM Offre WHERE id_candidat IS NULL;
```

Enfin pour tester des patterns de chaines de caractères on peut utiliser le mot clé `LIKE`

```sql
-- Ne retourne que les tuples de la table bien qui ont 'calme' en lowercase dans leur description
-- % signifie "n'importe quel chaine de caractère"
-- _ signifie "n'importe quel caractère"
SELECT * FROM Bien WHERE description LIKE '%calme%';

-- Ici on veut tous les tuples de Bien qui ont le symbole % dans leur description, 
-- mais % est un caractère de syntaxe de LIKE donc on doit l'escape ici on choisis \
-- Si on veut après escape '\' alors on peut simplement le mettre deux fois '\\'
SELECT * FROM Bien WHERE description LIKE '%\%%' ESCAPE '\';
```
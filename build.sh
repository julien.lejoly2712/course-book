#!/bin/sh

pwd
for dir in */
do
    ln -s $(pwd)/theme/ $(pwd)/${dir}theme/
    $(pwd)/mdbook build "$(pwd)/$dir"
done

# Ma procédure de CSS pour mon projet de devweb
Pour la première eval du projet j'étais grave à la bourre j'ai vraiment commencé mon projet 5 jours avant la deadline et j'ai changé de design en cours de route et tout recommencé de zéro plusieurs fois.

Mais la méthode qui marchait le mieux pour être organisé (en tout cas pour moi) c'était d'avoir une procédure à suivre rigoureusement pour chaque élément du site. Et donc faire les choses au fur et à mesure pour ne pas être submergé. Si je ne faisais pas ça, rien n'étais organisé et tout s'éffondrait à la première modification venue, donc pas top.

Avec cette procédure, ça m'a permit de faire un site en utilisant flexbox et qui est aussi totalement responsive (à quelques détails près). 

Je n'ai pas utilisé SASS pour ce projet mais à bien y réfléchir ça n'aurait peut'être pas été une mauvaise idée car la syntaxe est bien plus simple.

## Préparer le CSS
1. Ajouter un nouveau fichier `style.css` dans un dossier `css` et y ajouter le contenu suivant pour faire en sorte de se simplifier la vie par après

```css
* {
    box-sizing: border-box;
    margin: 0px;
    padding: 0px;
    font-family: "nom de la police"; /* Ajout de(s) la police(s) par défault */
}
```

2. Ajouter la ligne suivante dans tous les `<head>` de toutes les pages pour un bien meilleur rendu sur mobile :

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0">
```

3. Ajouter la ligne suivante dans tous les `<head>` de toutes les pages pour lier la feuille de style

```html
<link rel="stylesheet" href="css/style.css">
```


## Design de l'élément pour mobile (mobile first pour être responsive)
1. Faire le style élément par élément pour ne pas devenir fou. Et utiliser l'éditeur de style du navigateur pour s'aider et avoir les changements en temps réel. **Mettre le navigateur en mode "téléphone"**

```css
.classeDeLelementOuSelecteur {
  /* Code suivant ici */
}
```

2. Si l'élément contient d'autres éléments, ajouter un flex

```css
display: flex;
flex-direction: column; /* Définir la direction des éléments "row" pour horizontal et "column" pour vertical */
```

3. Définir la taille (en % pour que ce soit relatif) si nécessaire

```css
width: 50%;
```

4. Ajouter des marges, pour centrer un élément, utiliser `auto`

```css
margin: auto; /* Centrer un élément */
margin: 10px auto; /* Centrer un élément horizontalement et mettre une marge de 10px en haut et en bas */
margin: 0px auto 0px 0px; /* Aligner un élément à gauche en mettant une marge auto à droite */
margin: 0px 0px 0px auto; /* Aligner un élément à droite en mettant une marge auto à gauche */
```

5. Ajouter du padding pour "grossir" l'intérieur d'un élément (le padding est une marge interne entre le contenu et la bordure de l'élément)

```css
padding: 10px;
```

6. Ajouter une bordure et des bords arondis

```css
border: solid black 3px; 
border: dashed black 3px;

border-radius: 10px;
```

7. Annuler le style automatique des liens (`<a>`) et des listes (`ol` et `ul`) :

```css
text-decoration: none;
list-style: none;
```

8. Définir une couleur du texte et du fond

```css
color: white; /* Couleur du texte */ 
background-color: black; /* Couleur du fond */
```

9. Définir la police, le niveau de "gras", l'alignement du texte et la taille de la police (utiliser des rem pour cela pour avoir une unité relative)

```css
font-family: Roboto;
font-weight: 700;
text-align: center;
font-size: 1.5rem;
```

10. Changer le curseur pour que cela soit un curseur pointeur ("la main")

```css
cursor: pointer;
```

## Design de l'élément quand on passe la souris dessus (:hover)
1. Ajouter le même sélecteur avec ":hover"

```css
.classeDeLelementOuSelecteur:hover {
  /* Code suivant ici */
}
```

2. Changer la couleur si nécessaire

```css
color: black;
background-color: lightgrey;
```

3. Ajouter une transition pour que l'élément bouge quand on met la souris dessus

```css
transition: ease-in-out;
transition-duration: 300ms;
transform: translateY(-5px);
```

## Design de l'élément sur PC
1. Quitter le mode "téléphone" mis précédemment, mais ne pas oublier de le réactiver après. Ajouter dans le sélecteur `@media` si il existe, sinon le créer :

```css
@media (min-width: 1024px) {
  .classeDeLelementOuSelecteur {
    /* Code suivant ici */
  }

  /* Autres classes et sélecteurs peuvent s'imbriquer ici aussi */
}
```

2. Changer la direction du flex si nécessaire 

```css
flex-direction: row;
```

3. Définir une taille limite si besoin

```css
width: 50%;
```

4. Ajouter une marge / centrer :

```css
margin: auto;
margin: 20px;
/* ... comme vu précédemment */
```

5. Ajouter d'autres propriétés, si besoin voir la première procédure.

## Enregistrer les changements
1. Copier le code CSS dans le fichier CSS pour le sauvegarder 
2. Répèter la procédure pour chaque élément de chaque page. Toujours le faire au fur et a mesure pour ne pas devenir fou. Et ne pas oublier le *mobile-first*

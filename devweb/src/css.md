# Introduction au CSS
> Note: Ici on ne parle que de CSS pure, sans framework

## Les différents emplacements où mettre du CSS

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
        <style>
        .pire-police {
            font-family: "Comic Sans MS";
        }
        </style>
    </head>
    <body>
        <p style="font-family: "Comic Sans MS">Mon texte dans la pire police d'écriture que cet univers ai jamais connu</p>
        <p class=".pire-police">La même chose que le paragraphe précédent</p>
        <p class=".encore">Toujours la même chose</p>
    </body>
</html>
```

On peut mettre du code CSS à plusieurs endroits différents :

* Dans la propriété `style` d'un élément. Comme dans le premier exemple.
* Dans le `head` du fichier en pointant le/les éléments à l'aide d'un sélecteur (voir plus tard). On le met alors dans des balises `<style>`
* Dans un fichier externe ici "style.css" dans une balise `<link>` elle même dans le `<head>`. 

C'est la manière la plus commune de faire car elle permet d'avoir des fichiers plus courts, de devoir moins écrire et de le réutiliser sur plusieurs fichiers HTML différents.

## La notion d'héritage

```html

```

## Le viewport
## Les sélecteurs

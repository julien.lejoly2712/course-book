# Summary

- [Coucou](coucou.md)
- [Mes chosees à faire](todo.md)
- [Tutoriels]()
    - [Tutoriels relatifs à l'infrastructure de l'école](HelmoTuto.md)
    - [Créer son propre VPN (Wireguard) avec un Raspberry PI](vpn.md)
    - [Créer son propre site statique (comme celui ci)]()
    - [Utiliser le clavier et souris d'un ordinateur pour en controler plusieurs en même temps](barrier.md)
    - [Créer un réseau IRC de A à Z](irc.md)
    - [Rice de bspwm](bspwm.md)
    - [Monter un serveur comme une clé USB sur son PC (remplacement à Filezilla)](sshfs.md)

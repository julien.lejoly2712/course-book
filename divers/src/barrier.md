# Comment utiliser Barrier
Barrier est un logiciel qui permet d'utiliser le clavier, la souris et le presse-papier d'un ordi pour en controler d'autres.

Pour cela il faut configurer un ordinateur comme *serveur* et puis on peux ajouter autant de *clients* que l'on veut.

## Installer Brarrier
* Sur Linux, on peux installer `barrier` ou `barrier-headless`, sur Windows et macOS il faut télécharger le fichier EXE [depuis GitHub](https://github.com/debauchee/barrier/releases)

```bash
sudo apt install barrier-headless
```

## Configurer le serveur
* Ouvrir le fichier `~/.local/share/barrier/.barrier.conf`

```bash
nano ~/.local/share/barrier/.barrier.conf
```

* Coller la configuration suivante (pour trois appareils)

```bash
section: screens
	left_device:
		halfDuplexCapsLock = false
		halfDuplexNumLock = false
		halfDuplexScrollLock = false
		xtestIsXineramaUnaware = false
		preserveFocus = false
		switchCorners = none
		switchCornerSize = 0
	middle_device:
		halfDuplexCapsLock = false
		halfDuplexNumLock = false
		halfDuplexScrollLock = false
		xtestIsXineramaUnaware = false
		preserveFocus = false
		switchCorners = none
		switchCornerSize = 0
	right_device:
		halfDuplexCapsLock = false
		halfDuplexNumLock = false
		halfDuplexScrollLock = false
		xtestIsXineramaUnaware = false
		preserveFocus = false
		switchCorners = none
		switchCornerSize = 0
end

section: aliases
end

section: links
	left_device:
		right = middle_device
	middle_device:
		right = right_device
		left = left_device
	right_device:
		left = middle_device
end

section: options
	relativeMouseMoves = false
	screenSaverSync = true
	win32KeepForeground = false
	clipboardSharing = true
	switchCorners = none
	switchCornerSize = 0
end
```

* Prendre l'IP locale du serveur

```bash
hostname -I 
# ou 
ip addr
```

## Connecter des clients au serveur
On peut maintenant se connecter au serveur en utilisant les commandes suivantes en fonction des besoins:

```bash
# Barrier sans SSL, en mode "debug", en tant que "middle_device"
barrierc -f -d DEBUG --disable-crypto -n middle_device <IP ADDRESS>

# Barrier sans SSL, en tant que "middle_device"
barrierc --disable-crypto -n middle_device <IP ADDRESS>

# Barrier sans SSL, avec le nom du PC par default (nécessite de changer la configuration)
barrierc --disable-crypto <IP ADDRESS>
```

Toutes ces options peuvent être définie par l'interface graphique (si installée) mais perso je préfère les commandes.

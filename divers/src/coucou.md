# Snow's site
Coucou, moi c'est Julien. Voilà. Ce site contient principalement des tutoriels et des notes de cours. 

Sur ce site vous trouverez, entre autre, des synthèses (voir ci dessous), des tutoriels sur des trucs random, des liens vers d'autres projets.

## Raccourcis vers d'autres sites
* [Helmo Learn](https://learn-technique.helmo.be)
* [Serveur Discord *Helmo - Informatique/Sécurité*](https://discord.gg/3vWtK5QMcw)
* [Mails Helmo](https://outlook.office.com/)
* [Helmo Connect](https://connect2.helmo.be/TableauDeBord/Etudiant/Index)
* [La Swilabible](https://swilabible.be)
* [Swikipedia](https://swikipedia.be)

## Notes de cours / synthèses
* [Java - Programmation de base](../../java/book)
* [Architecture des ordinateurs](../../archi/book)
* [Développement web](../../devweb/book)
* [Analyse](../../analyse/book)
* [Base de données](../../bdd/book)
* [Mathématiques appliquées à l'informatique](../../math/book)

## Autre trucs
* [Linux et CLI](../../linux/book)
* [DevOps](../../sysadmin/book)

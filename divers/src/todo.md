# Trucs à faire
> Ceci est une page perso, si vous n'êtes pas moi, cliquez [ici](https://www.youtube.com/watch?v=p7YXXieghto).

## A faire
* (10/11/22) Ajouter une nouvelle synthèse d'archi sur les ALU, additions, etc.
* (10/11/22) Trouver des vidéos explicatives de la conversion entre bases et autres en Archi 
* (10/11/22) Terminer les différents points en archi (voir SUMMARY)
* (14/11/22) Lister les différents points appris jusque là en BDD + labo
* (15/11/22) Améliorer le tuto Git en se concentrant uniquement sur ça
* (16/11/22) Ajouter un menu au projet dev web et l'héberger sur Git
* (20/11/22) Améliorer le thème de ce site

## A faire ou en cours (plus urgent)
* (10/11/22) Commencer les synthèses de français

## Terminé
* (08/11/22) Lister les différents points appris jusque là en analyse et faire le sommaire de la synthèse
* Faire la synthèse d'analyse
* Voir pour le cours de programmation de base
* Voir pour les cours de RIEP
* Faire la synthèse d'archi
* Configurer un autre PC de remplacement
* Ajouter des images/vidéos pour le cours d'[archi](archi.html)
* Installer les machins (IDE, OpenJDK 18, Eclipse, )
* Écrire le début du résumé de [Java](Java.html)
* Résumer le premier cours d'archi
* Mettre des couleurs dans les blocs de code de ce site + math stuff
* Lire la FAQ du cours d'anglais & aller voir le syllabus
* Trouver une alternative sympa à `dwm`
* Écrire la documentation du script de ce site
* Refaire le CSS de ce site
* S'incrire au cours de programmation de base (b1info)

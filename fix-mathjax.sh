#!/bin/bash
cp -r ~/theme/ .
cd src/
sed -i 's/\\\\(/$/g' *
sed -i 's/\\\\)/$/g' *
sed -i 's/\\\\\[/$$\n/g' *
sed -i 's/\\\\\]/\n$$/g' *
echo "Mathjax fixed"

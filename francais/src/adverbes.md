# Les adverbes en -ment dérivé d'adjectifs
Si l'adjectif d'origine fini par **ent** alors l'adverbe finira par **emment**

> Exemple : _prudent_ → _prud**emment**_

En revanche si l'adjectif d'origine fini par **ant** alors l'adverbe finira par **amment**

> Exemple : _suffisant_ → _suffis**amment**_

Sinon on met l'adjectif au féminin et on ajoute **ment**

> Exemple : _clair_ → _clair**e**_ → _claire**ment**_

**⚠️ Exceptions** : 

* _bref_ → _brièvement_
* _grave_ → _grièvement_ (souvent en rapport avec le contexte médical)


## Quelque vs quel que vs quelques 

| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| *quelque* invariable | _environ_ ou _aussi_ | Si suivi par un nombre ou un adjectif | _Quelque **300** euros_ ou _Quelque **jolies** que vous soyez_ |
| *quel que*, *quelle que* |  | Si suivi par le verbe être (ou un équivalent) et accordé au sujet | _Quelle que **soit** la difficulté_ ou _Quelles que **puissent être** les difficultés_ |
| *quelque* invariable | Si on peut remplacer par "quelconque" | Quand on parle de quelconque XYZ, _quelque_ est invariable et le CDV est mis au singulier. | _J'ai rencontré quelque **chien**._ (_un quelconque_) |
| *quelques* |  | Si suivi par un nom au pluriel (qui n'est pas quelconque comme vu précédemment) | _Quelques **amis**_ ou _Quelques **livres**_ |

## Leur vs leurs

| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _leur_ | Si on peut remplacer par _lui_ | Invariable quand il est un pronom | _Je **leur** téléphone_ (== _Je **lui** téléphone_) |
| _leur_, _leurs_ | Tous les autres cas | Quand il est un adjectif, s'accorde avec le nom qu'il précise | _Je connais bien **leurs** amis._ ou _J'ai vu **leur** voiture._ |

## Tout vs tous
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _tous_, _toutes_ |  | Quand il est pronom (il est le sujet de la phrase et remplace un nom), le verbe s'accorde à lui | _Tous sont venus._ ou _Tout**es** étaient accompagné**es**_ |
| _tous_, _toutes_ |  | Quand il est un adjectif, il s'accorde au nom qu'il qualifie | _Tous les invités sont venus_ ou _Tout**es** l**es** personn**es** étai**ent** accompagné**es**_ |
| _tout_ | _entièrement_ | Quand il adverbe, il reste invariable. **Sauf dans certaines exceptions où il s'accorde avec le sujet** | _Ils sont restés **tout** bêtes._ ou _Elles sont restées **toutes** bêtes_ |
| _tout_ | _ensemble_ | Quand il signifie _un ensemble_ il est un nom | _Des **touts** distincts_ (_des **ensembles** distincts_) |

## Quoi que vs quoique
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _quoique_ | _bien que_ |  | _**Quoique** ce soit difficile_ | 
| _quoi que_ | _qu'importe ce que_ |  | _**Quoi que** tu fasses..._ |

## Peut-être vs peut être
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _peut-être_ | _probablement_ | Quand il est adverbe, on met un tiret entre les deux mots | _Il est **peut-être** temps._ |
| _peut être_ | _pourrait être_ | Quand il est le verbe, on ne met pas de tiret entre les deux mots | _Il **peut être** distrait._ |

## Quant à/au·x vs Quand
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _quant à/au·x_ | _pour qui est de_ | Sert à isoler quelqu'un ou une affirmation | _**Quant** aux autres exercices, vous les ferez plus tard._ |
| _quand_ | _à quel moment ?_ ou _lorsque_ dépendant de si c'est une question | Exprime le temps, la simultanéité ou la cause | _**Quand** viens-tu ? **Quand** je serai prêt._ | 

## Près vs prêt·e·s
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _près_ | _pas loin de_ | Est une locution adverbiale (invariable) et est **toujours accompagné de _de_** | _La direction est **près d**'abandonner_ |
| _prêt·e·s_ | _disposé·e_ | Est un adjectif (variable) et est **toujours accompagné de _à_** | _La direction est **prête à** abandonner_ | 

## Davantage vs d'avantage·s
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _davantage_ | _(encore) plus_ | Est un adverbe (invariable) | _J'irai **davantage** courir quand il fera beau._ |
| _d'avantage·s_ | _de gains_ | Préposition _de_ + nom _avantage·s_ | _Je ne vois pas **d'avantages** à cette situation_ |

## Voir vs voire
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _voire_ | _ou encore_ | Est une conjonction | _Je n'y suis plus allé depuis des semaines, **voire** des mois._ | 
| _voir_ | _regarder_ | Est un verbe | _J'ai prévu de **voir** ma grand-mère_ |

## Même vs mêmes
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _même_ | _également_ | Il est adverbe (**invariable**) | _Elles insistent **même** pour que tu restes !_ |
| _même·s_ | _identique·s_ ou _idéal·e·s_ | Il est adjectif (variable) et s'accorde avec ce qu'il qualifie | _Nous avons les **mêmes** habitudes_ ou _Ma grand-mère est l'intelligence et la sagesse **mêmes**_ |
| _-même·s_ |  | Quand il suit un pronom personnel, il est précédé d'un trait d'union et s'accorde au pronom | _Ils fabriquent eux-**mêmes** leur matériel._ |
| _la/le·s même·s_ |  | Quand il est précédé d'un déterminant, il s'accorde avec le nom qu'il remplace | _J'ai **les mêmes** à la maison_ |

## Ou vs où
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _ou_ | _ou bien_ | Indique une conjonction de coordination | _Veux tu du chocolat **ou** des fraises ?_ | 
| _où_ |  | Indique un lieu | _**Où** habites-tu ?_ | 

## La vs là
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _la_ | | C'est un déterminant défini | _**La** porte est fermée_ |
| _là_ | | C'est un adverbe de lieu | _C'est **là** que j'habite._ | 
| _-là_ | _-ci_ | Ça sert à désigner une chose précise | _C'est ce soir-**là** que je me suis cassé une jambe_ |
| _là-_ | _ici-_ | Compose un adverbe de lieu | _**Là**-bas, **là**-haut_ |

## Demie vs demi vs demis vs demies
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _demi-_ |  | En tant qu'adjectif ou adverbe s'il est devant un nom avec un trait d'union, alors il reste **invariable** | _Une **demi**-journée_ ou _Du lait demi-écrémé_ |
| _et demi·e_ |  | En tant qu'adjectif, si il est après un nom avec _et_, alors il s'accorde au nom | _Six heures et **demie**_ |
| _à demi_ |  | En tant qu'adverbe, demi reste **invariable** | _Elle est **à demi** sourde._ |
| _un/une/la/les demi·e·s_ |  | En temps que nom _demi·e_ peut représenter la moitié d'une unité, ou la moitié d'une heure donnée (peu courant) | _Trois **demis** pression_ ou _Il est la **demie** de trois heures_ |

## A vs À
| Forme | Peut être remplacé par | La règle | Exemple |
| --- | --- | --- | --- |
| _a_ | _avait_ | Est le verbe avoir à la troisième personne du présent | _Elle **a** du pain._ | 
| _à_ |  | Peut signifier [beaucoup de choses dépendant du contexte](https://fr.wiktionary.org/wiki/%C3%A0) | _On se voit **à** 8 heures._ | 


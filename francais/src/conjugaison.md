# Conjugaison
Les formes verbales reflètent à la fois un _mode_ et un _temps_

| Mode | Description | Passé | Présent | Futur |
| ---- | ----------- | ----- | ------- | ----- |
| Indicatif | Réalité | _J'**ai été**_ | _Je **suis**_ | _Je **serai**_ |
| Subjonctif | Subjectivité, nécessité, jugement de valeur, etc (**toujours introduit par _que_**)| _Il faut que j'**aie été**_ | _Il faut que je **sois**_ | Aucun |
| Conditionnel | Hypothèse | _J'**aurais été**_ | _Je **serais**_ | Aucun |
| Infinitif | Degré 0 du verbe | _**avoir été**_ | _**être**_ | Aucun |
| Impératif | Ordre | _**Aie fini** ton travail avant 15h_ | _**Finis** ton travail._ | Aucun |

## Indicatif
- Les verbes en **-ger** → le radical le change pas seulement les terminaisons changent

> Exemple : _je **mange**, tu **mange**s, iel **mange**, nous **mange**ons, vous **mange**z, iels **mange**nt_

- Les verbes au passé simple. Il ne faut pas les confondre avec le passé composé. Le passé composé c'est _auxiliaire + participe passé_ tandis que le passé simple n'a qu'un seul élément.

> Exemple : _il a véc**u**_ - _je vécus, tu vécus, il véc**ut**_ et _il a pr**is** - je pris, tu pris, il pr**it**_ 

- Pour l'imparfait les verbes en -ier paraissent bizarres. Car on garde le radical.

> Exemple : _je **ni**ais, tu **ni**ais, iel **ni**ait, nous **ni**ions, vous **ni**iez, ils **ni**aient_

- Attention à ne pas confondre l'indicatif futur (futur simple) et le conditionnel présent. 

> Exemple : conditionnel → _je devrais_ vs indicatif futur → _je devrai_

- Les verbes en -yer ont une forme simplifiée à l'indicatif présent et futur

> Exemple : _~~J'essaye~~_ → _J'essaie_ → _J'essaierai_

## Subjonctif
- Les verbes au subjonctif à la troisième personne du singulier prennent un accent circonflexe (contrairement à l'indicatif passé simple)

> Exemple : Ind. Passé simple → _Il f**u**t_, Ind. Subjonctif → _Il fallait qu'il f**û**t_

- La finale est toujours **-e, -es, -e, -ions, -iez, -ent**

> Exemple : _Que j'aille, que tu ailles, qu'iel aille, que nous allions, que vous alliez, qu'iels aillent_

- **⚠️ ATTENTION** : Sauf pour être et avoir

> Exemple : _que je soi**s**, que tu soi**s**, qu’iel soi**t**, nous so**y**ons, vous so**y**ez_ et _qu’il ai**t** , nous a**y**ons, vous a**y**ez_

## Conditionnel

| Phrase subordonnée | Phrase principale | Exemple |
| --- | --- | --- |
| Si + indicatif présent | Indicatif présent ou futur | _Si tu **es** d'accord, nous **partons**_ ou _Si tu **es** d'accord, nous **partirons**_ |  
| Si + indicatif imparfait | Conditionnel présent | _Si tu **étais** d'accord, nous **partirions**_ |
| Si + indicatif plus-que-parfait | Conditionnel passé | _Si tu **avais été** d'accord, nous **serions partis**_ | 

**⚠️ Attention** : _~~Si j'au**rais**~~_ → _Si j'av**ais**_, _~~Si je fe**rais**~~_ → _Si je fai**sais**_

## Infinitif
## Impératif
- Pas de « s » à la 2e personne du singulier avec les verbes en -e et avec le verbe aller 

> Exemple : _Tu manges_ → _mange !_ et _tu vas_ → _va !_

**⚠️ Attention** : On met un « s » pour l’euphonie (la mélodie, avec une liaison). 

> Exemple : _Passe à la maison !_ → _passes-y !_ et _Tu y vas._ → _Vas-y !_

- Quand le prnom est placé derrière le verbe, il prend un tiret

> Exemple : _Tu t'en vas_ → _Va-t’en !_ et _Tu le prends_ → _Prends-le_

# Les couleurs simples et composées
Une couleur, quand elle n'est pas composée, s'accorde toujours au nom qu'elle qualifie.

> Exemple : _Un pull **bleu**_, _Deux pull **bleus**_, _Une écharpe **bleue**_, _Deux écharpes **bleues**_

Pour ce qui est des couleurs qui correspondent à des choses (tel que _orange_, _kaki_, etc), on **n'accorde pas**. 

> Exemple : _Deux écharpes **orange**_

**⚠️ Exception** : _rose·s, mauve·s, vermeil·s, écarlate·s, pourpre·s, violet·te·s, fauve·s_ eux s'accordent.

> Exemple : _Deux écharpes **roses**_

En revanche pour les couleurs faites avec plusieurs éléments : **on n'accorde jamais**

> Exemple : _Deux pulls **vert clair**_, _Deux parapluies **rouge et or**_, _Des lettres **lie-de-vin**_


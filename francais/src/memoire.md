## Les choses à mémoriser telles quelles
Il n'y a pas vraiment de règle pour cette liste, c'est juste des mots sur lesquels on se trompe souvent.

- plupart
- magasin
- magazine (de magasin, qui signifie stock → 'magasin pittoresque', stock de nouvelles → Angleterre: _magazine_ → France: _magazine_)
- pour ce faire = pour faire cela (cela = ce)
- suffisamment 
- je serai, tu seras 
- gentiment
- gaiement
- vêtement
- exigeant (toujours ainsi)
- précautionneusement
- seize = 16
- soixante = 60
- ne pas confondre : elle s’est enfuie (verbe s’enfuir) et elle est en fuite
- un problème pécuniaire
- par conséquent / en conséquence
- en définitive
- en bonne et due forme
- auprès 
- un différend = un conflit
- la manière dont (car d’une certaine manière)
- _C'est le cas, et ce, dès lundi_ : toujours virgule devant et derrière « et ce »
- _Telle mère, telle fille_ → tel, telle, tels, telles
- _Quel, quels, quelle, quelles_

## Verbes à mémoriser
- Peler → _je pèle, nous pelons, iels pèlent._
- Appeler → _j’appelle, nous appelons, vous appelez, iels appellent_
- Jeter → _je jette, tu jettes, iel jette, nous jetons, vous jetez, iels jettent_
- Acheter → _j’achète, nous achetons, iels achètent_
- Verbes en -aincre → _je vaincs, tu vaincs, iel va**inc**_
- Promouvoir → _je promeus, il promeut, nous promouvons, iels promeuvent_
- Conna**î**tre → _je connais, tu connais, iel conna**î**t, nous connaissons_ (le ^ remplace le _s_)
- Résoudre → _je résous, tu résous, iel résou**t**, nous résolvons, vous résolvez, iels résolvent_
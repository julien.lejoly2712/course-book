# Les natures et fonctions
Un même mot peut posséder plusieurs sens et donc plusieurs natures. 


Par exemple :

> Le `tout` est de savoir si `tous` les exercices sont de `toutes` natures et sont `tout` nouveaux.

Chaque variation de "tout" ou "tous" a un sens différent dans la phrase. 


## Les natures (ou classes)

Il en existe 2 types, les variables et les invariables. Les variables s'orthographient différemment en fonction du genre et du nombre, tandis que les invariables s'orthographient toujours de la même façon.

| Variables | Invariables |
| --- | --- |
| **Nom** | Adverbe |
| Déterminant | Conjonction |
| Adjectif | Préposition |
| **Pronom** | |
| Verbe | |

*Les deux en gras "nom" et "pronom" sont les donneurs d'accord. Cela veut dire que si ceux-ci sont féminins par exemple, alors les adjectifs associés seraient féminins aussi par exemple.*

Pour voir avec plus en détail chaque classe...

| Classe | Description | Exemple(s) de phrase(s) | Types |
| --- | --- | --- | --- |
| **Nom** | Les noms *communs* sont variables tandis que les noms *propres* sont toujours invariables. | Je mange des `frites`. Et toi `Schumi` ? | |
| **Pronom** | Le pronom sert à remplacer un mot ou un groupe de mots. | Schumi a fait une erreur ! `Il` est probablement victime de l'effet boulet-frites. | |
| **Verbe** | Le verbe sert à exprimer des actions, des états et des changements les situant dans le temps. | Schumi a `mangé` des frites. | |
| **Déterminant** | Le déterminant sert à introduire un nom | `Les` frites ont diminué ses pouvoirs. |  |
| **Adjectif** | Un adjectif sert à caractériser, qualifier ou préciser un nom ou un pronom. | Au moins, les frites étaient `bonnes`. |  |


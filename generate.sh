#!/bin/bash
cd src/
rm xx*.md SUMMARY.md
#csplit -b "%02d.md" $1 /##/ {*}
csplit -b "%02d.md" $1 /^##\ / {*}

rm xx00.md

for i in {1..100}
do
    echo "$i"
#    header=$(cat $1 | grep "##" | sed 's/^#* //g' | cut -d$'\n' -f$i)
    header=$(cat $1 | grep "^## " | sed 's/^## //g' | cut -d$'\n' -f$i)
    filename=$(ls xx* | cut -d$'\n' -f$i)
    echo "- [$header]($filename)" >> SUMMARY.md
    echo "- [$header]($filename)"
done

sed -i '/- \[\]()$/d' SUMMARY.md

# Remove details and summary things in math courses
sed -i 's/<details><summary>/## /g' *
sed -i 's/<\/summary>//g' *
sed -i '/<\/details>/d' *

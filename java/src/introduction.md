## Introduction
Ceci sont mes notes de Java. J'ai essayé de les écrire sous le même format que [Rust by example](https://doc.rust-lang.org/stable/rust-by-example/).

Vous pouvez trouver mes solutions et celle d'autres personnes sur mon Git :

* [codeberg.org/SnowCode/prb](https://codeberg.org/SnowCode/prb)

Pour sélectionner les personnes ou différentes versions du code, changer la "branche" Git.

Bonne lecture, amusez vous bien :)

# Regex

Voici un petit résumé de la signification des différents caractères :

| Element en regex | Signification | 
| --- | --- | 
| `\` | Indique que le caractère qui suit est litéral et qu'il ne faut pas qu'il soit interpreté comme synatxe du regex |
| `^` | Début de la chaine de caractères |
| `$` | Fin de la chaine de caractères |
| `*` | Match le caractère précédent 0 fois ou plus |
| `+` | Match le caractère précédent 1 fois ou plus |
| `?` | Match le caractère précédent 0 ou 1 fois |
| `.` | Match n'importe quel caractère |
| `[abc]` | Match n'importe lequel des caractère dans les crochets  |
| `[A-Z]` | Match n'importe quel caractère dans une série (ici allant de A à Z majuscule) |


# Fonctionnement général du CLI
Le CLI (Command Line Interface, ou terminal ou invite de commande) est un outil très puissant fonctionne en lançant des genres de formules magiques à son ordinateur.

Contrairement à l'utilisation classique avec une souris, le terminal fonctionne principalement avec le clavier. Cela permet d'être beaucoup plus rapide et de pouvoir optimiser énormément de choses.

Mais tout d'abord on va d'abord voir les bases.

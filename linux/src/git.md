# Git
Git est un un logiciel libre de gestion de versions décentralisé créé par Linus Torvalds (créateur de Linux).

> Le magazine PC World nous apprend que « quand on lui a demandé pourquoi il avait appelé son logiciel “git”, qui est à peu près l'équivalent de “connard” en argot britannique, Linus Torvalds a répondu “je ne suis qu'un sale égocentrique, donc j'appelle tous mes projets d'après ma propre personne. D'abord Linux, puis Git.” ».

Il ne faut pas confondre Git avec GitHub. GitHub est un site qui héberge des projets Git, mais ce n'est pas Git en soi. Je vais y venir plus tard.

## A quoi sert Git
Git est un logiciel de gestion de versions, il permet d'avoir un historique des changements dans un dossier (partiulièrement utilisé pour le code mais pas que, cela peut aussi être utiliser pour gérer des wikis).

Git permet ainsi une collaboration plus simple et une meilleur gestion de projet car cela évite de devoir se retrouver avec des "monCode1.java, monCode-cette-fois-ci-ca-marche.java, monCode3.java, monCodeJenAiMarreDeProgrammer.java, etc".

## Installation de Git
Sur linux, il suffit d'installer le paquet `git`, il est même possible que Git soit déjà installé. 

Si vous n'êtes pas sur Linux, vous pouvez télécharger le fichier d'installation depuis [ce site](https://git-scm.com/downloads) ou via un gestionaire de paquet déjà installer comme `chocolatey` ou `brew`.

## Utilisation de base de Git
Tout d'abord on va intier un nouveau projet git du nom de "mon-super-projet" puis on va aller dedans en utilisant `cd`. On peut aussi transformer un dossier existant en projet git en allant dedans et en lançant `git init` tout seul.

```bash
git init mon-super-projet
cd mon-super-projet
```

Ensuite on va créer un nouveau fichier (vous pouvez créer le fichier que vous voulez dans ce cas ci je vais créer un fichier `README.md` avec `nano`)

```bash
nano README.md
```

Une fois que j'ai ajouté des trucs je peux ajouter tous mes changements avec `git add`

```bash
git add README.md
```

On peut maintenant "archiver" cette version du code (faire un commit) auquel on pourra revenir si besoin plus tard. On va préciser l'option `-m` pour décrire nos changements.

```bash
git commit -m "J'ai ajouté un fichier :)"
```

Et voilà :) On a maintenant un répo git fonctionnel. Mais on va aller un peu plus loin.

### Publier son code sur internet
Pour publier son code (ou autres fichiers) on peut se créer un compte sur une "forge" git. Il y a beaucoup d'options pour cela, mais en voici quelque uns:

* [GitHub](https://github.com), le plus connu, mais pas forcément le meilleur.
* [GitLab](https://gitlab.com), le deuxième plus connu, mais c'est pas ma tasse de thé non plus.
* [Codeberg](https://codeberg.org), mon préféré de loin. Très simple, rapide, avec tout ce qui faut. En plus c'est un logiciel libre.

Ensuite, on peut cliquer sur le bouton pour créer un nouveau répo. 

On peut maintenant retourner dans notre projet et ajouter un `remote` de type `origin`. Il suffit d'utiliser l'URL ou la commande donnée sur le site.

```bash
git remote add origin https://codeberg.org/SnowCode/test.git
```

Enfin, on peut publier les changements en utilisant la commande `push`

```bash
git push -u origin master
```

Maintenant si on retourne sur le site, on peut y voir notre code là bas. 

### Collaborer sur le code de quelqu'un d'autre
Si on va sur la page d'un projet Git sur une forge, on peut souvent y voir un bouton "Fork", en cliquant dessus, on va créer une copie du code sur notre utilisateur. On peut télécharger et modifier ce code. Pour télécharger du code depuis un repo git distant, on utilise la commande `git clone`

```bash
git clone https://codeberg.org/SnowCode/test.git
```

On peut maintenant modifier ce que l'on veut et de nouveau utiliser les commandes git `add`, `commit` et `push`.

```bash
cd test
nano README.md
git add README.md
git commit -m "Rectification d'une faute d'orthographe"
git push -u origin master
```

Quand on retourne sur le site, on peut aller sur le projet de base, aller dans la section `Pull Requests` et en créer une nouvelle. Une pull request permet de créer une proposition de fusion de votre code avec le code d'origine. Si elle est acceptée, les changements seront automatiquement ajoutés.

Une autre chose possible sur ces sites est de reporter des problèmes avec le code ou encore de proposer des modifications. Cela s'appelle des "issues", c'est un genre de forum de discussion à propos d'un projet.

Si quelqu'un a collaboré sur votre projet, vous devez le re-synchroniser. Pour cela vous pouvez simplement lancer la commande `git pull`

```bash
git pull
```

Essayez d'éviter de collaborer à plusieurs personnes sur le même fichier car cela peut créer des conflits.

### Revenir dans le temps
Maintenant on peut un peu regarder comment revenir dans le temps, vers une version précédente de notre code. Tout d'abord on va voir la liste des changements :

```bash
git log
```

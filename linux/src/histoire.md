# Le logiciel libre, la culture libre et les hackers
> **Cette page est en cours de construction**, ce sujet est assez difficile a aborder tellement il est vaste et important pour moi. Donc ca risque de prendre du temps :)

## La culture libre avant internet

### Le mundaneum
En 1910, Paul Otlet et Henri La Fontaine fondent le Mundaneum, une institution qui avait pour vocation de collecter le savoir mondial. 

Paul Otlet était un pacifiste utopique qui souhaitait arriver à la paix mondial à travers la communication de l'information la plus libre et accessible possible. 

En 1934, Paul Otlet écrit :

> On peut imaginer le télescope électrique, permettant de lire de chez soi des livres exposés dans la salle teleg des grandes bibliothèques, aux pages demandées d’avance. Ce sera le livre téléphoté.


> Ici, la Table de Travail n’est plus chargée d’aucun livre. À leur place se dresse un écran et à portée un téléphone. Là-bas, au loin, dans un édifice immense, sont tous les livres et tous les renseignements, avec tout l'espace que requiert leur enregistrement et leur manutention, [...] De là, on fait apparaître sur l’écran la page à lire pour connaître la question posée par téléphone avec ou sans fil. Un écran serait double, quadruple ou décuple s'il s'agissait de multiplier les textes et les documents à confronter simultanément ; il y aurait un haut parleur si la vue devrait être aidée par une audition. Une telle hypothèse, un Wells certes l'aimerait. Utopie aujourd'hui parce qu'elle n'existe encore nulle part, mais elle pourrait bien devenir la réalité de demain pourvu que se perfectionnent encore nos méthodes et notre instrumentation.

Cela fait beaucoup penser au web, ainsi son invention, le centre d'archive Mundaneum est parfois appellé "l'internet de papier".



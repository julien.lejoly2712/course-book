# Le logiciel libre, la culture libre et les hackers
> **Cette page est en cours de construction**, ce sujet est assez difficile a aborder tellement il est vaste et important pour moi. Donc ca risque de prendre du temps :)


> *D'un côté, l'information veut être chère, car elle est très précieuse. L'information adéquate dans le lieu approprié change simplement votre vie. D'un autre côté, l'information veut être libre, parce que le coût de son accessibilité est de moins en moins important avec le temps. Ainsi, on a ces deux conceptions qui s'opposent l'une à l'autre* ~ Steward Brand

> *Je crois que toute l'information d'utilité générale devrait être libre. Par "libre", je ne me réfère pas à son prix, mais bien plus à la liberté de copier l'information et de l'adapter aux nécessités propres à chacun* ~ Richard Stallman


## Quelques exemples de culture libre aujourd'hui
Vous connaissez et utilisez peut'être bon nombre de logiciels dit "libre" ou qui favorise la culture dite "libre". 

* Firefox 
* Notepad++ 
* GIMP 
* Inkscape
* TOR Browser
* VLC
* Audacity
* Blender
* 7-Zip
* Libreoffice
* Wikipedia
* Freemind
* Signal
* Bon nombre de mods, plugin et launchers Minecraft.

Dans le cadre des systèmes, réseaux et développement, là il y en a encore plus :

* Nginx, apache (serveurs web)
* Linux (Debian, Ubuntu, CentOS, Arch) (OS, surtout coté serveur)
* Beaucoup d'éditeurs de code (nano, vim, emacs, notepad++, atom, brackets, vscodium, etc)
* La plus part des languages de programmation (PHP, Java, Python, Rust, C, C++, C#, etc)

Mais il peut aussi y avoir de la culture libre de manière plus générale, bien plus que juste des logiciels :

* Le contenu de wikipedia, wikiversity, wikitionary, et bien d'autres
* Les archives Archive.org qui ont pour but d'archiver une très grande quantité d'information d'internet 
* Le site Sci-Hub et Library Genesis qui mettent a disposition gratuitement une très grande quantité de livres et publications scientifique
* Les licences Creative Commons, dont l'un des exemples le plus connu est [Kevin MacLeod](https://fr.wikipedia.org/wiki/Kevin_MacLeod_(musicien)) que vous connaissez déjà sans le savoir. Ou encore Wikipedia dont l'intégralité des pages sont sous cette licence. Ou encore la pluspars des cours HELMo.
* Le hardware libre, nottament les [Rep-Rap](https://fr.wikipedia.org/wiki/Rep_Rap), des imprimantes 3D open-source que l'on peut construire soi-même
* La médecine libre, a travers des informations sur les soins, ou la fabrication de médicaments. Nottament grace à des projets de fabrication de prothèses imprimées en 3D, d'insuline libre ([OpenInsulin Projet](https://openinsulin.org)), ou d'un projet universitaire pour combattre la malaria [OpenMalaria](https://openmalaria.org).
* LowTechLab Wiki et les low-techs en générales qui donne des guides pour créer soi-même diverses technologies respectueuses de l'environement, peu couteuses, utiles et durables.

Tous ces exemples font partie de la culture dite "libre". C'est a dire que leur code source, recette, plans, explications, ou autes sont disponibles gratuitement sur internet. Non seulement elles sont disponibles mais vous pouvez aussi les télécharger, les modifier, les utiliser comme bon vous semble, les redistribuer ou même proposer aux développeurs d'ajouter vos changements au projet principal et ainsi y contribuer directement. 

Le libre est fait dans l'idée de rendre le monde plus juste, en permettant concretement à n'importe qui de faire les choses soi même et d'être moins dépendant d'entreprises ou de gouvernements.

C'est l'idée que le partage d'information, ou le partage en général bénéficie à tous ceux qui y participent.

Pour Stallman, copier un programme n'est pas du vol, tout comme copier une recette de cuisine n'est pas du vol car c'est immatériel. Voler une voiture serait du vol, mais copier les plans pour construire cette dernière ne l'est pas.

La culture libre est donc intimement liée à la culture du *Do It Yourself*, je vais reparler de ça plus en détail sur le chapitre sur la culture Hacker.

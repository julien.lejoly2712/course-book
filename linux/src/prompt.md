# Le prompt et le fonctionnement général
Quand on ouvre pour la première fois un terminal on voit quelque chose comme ça :

```
snowcode@smol-computer:~$ _
```

Cela va probablement être différent pour vous. Dans mon cas, et si vous utilisez `bash` vous allez probablement voir la même chose, j'ai ceci

* `snowcode` est mon nom d'utilisateur. 
* `smol-computer` est le nom de mon ordinateur
* `~` est le chemin de fichier dans lequel je suis (on va voir après ce que ça signifie)
* `$` indique que je suis un utilisateur "normal", contrairement à un administrateur qui aurait le symbole `#`
* `_` est mon curseur


Cette première partie est appellée le "prompt", il ne sert qu'a indiquer des choses sur qui vous êtes et où vous êtes dans votre système.

C'est aussi à cet endroit que l'on peut lançer les commandes à notre ordinateur. Alors voyons quelques commandes très basiques:

* `ls` permet de lister les fichiers dans le dossier actuel
* `mkdir mon-dossier` permet de créer un nouveau dossier
* `cd mon-dossier` permet d'aller dans un dossier
* `pwd` permet d'avoir le chemin absolu du dossier actuel 
* `nano mon-fichier.txt` permet d'ouvrir un éditeur de texte pour modifier un fichier
* `touch autre-fichier.txt` permet de créer un 

# Calcul propositionnel
## Proposition

| Enoncé potentiel | Proposition ? | Raison | Valeur |
| --- | --- | --- | --- |
| Grand | Non | Trop ambigu | N/A |
| $ 4 = 9 $ | Oui | N/A | Faux |
| 8 | Non | Aucune comparaison | N/A |
| "Je vais gagner au lotto" | Non | Pas de certitude, pas de connecteur logique | N/A |
| $ 7 + 9 > 11 $ | Oui | N/A | Vrai |

Une proposition est un énnoncé dont on peut dire avec certitude s'il il est vrai ou non. Une proposition n'est donc pas ambigue et peut avoir seulement 2 valeurs (1 (vrai) ou 0 (faux)). 

# Summary

- [Les bases de Rust]()
    - [Introduction]()
    - [Installation et Hello World]()
    - [Affichage formatté]()
    - [Variables et types primitifs (et cast)]()
    - [Vecteurs et HashMap](hash-vec.md)
    - [Enums et gestion d'erreurs (Result et Option)](enum.md)
    - [Les conditions et les boucles]()
    - [Les fonctions (méthodes et closures)](fn.md)
    - [Les modules]()
    - [Structures, implementation & traits](impl.md)
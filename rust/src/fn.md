# Fonctions, méthodes et closures
> La différence entre une méthode et une fonction sera évoquée au chapitre sur [les implémentations](impl.md)

## Définition d'une fonction

```rust
fn ma_super_fonction() {
	// Cette fonction ne retourne rien
	println!("Hello World");
}

fn addition(a: i32, b: i32) -> i32 {
	// Cette fonction retourne un i32
	let result = a + b;

	// Il suffit d'indiquer le résultat sans terminer par un point virgule pour retourner une valeur
	result 
}
```

## Définition et utilisation des closures 
Les closures (ou "fermeture" en français) sont des fonctions anonymes qui peuvent être sauvegardées dans une variable et peuvent être passée comme argument à d'autres fonctions. 

Contrairement aux autres fonctions les closures peuvent également capturer des variables du contexte dans lequel elle est exécutée.

```rust
fn main() {
	let addition = |x, y| { 
		println!("Une addition est en cours...");
		x + y 
	};
	println!("1 + 1 = {}", addition(1, 1));
}
```

Certaines fonctions vous obligeront à utiliser des closures dans leurs arguments. Cela est représenté par `Fn`, `FnOnce` ou encore `FnMut`

Egalement quand on déplace des données vers une nouvelle tâche, on peut utiliser le mot clé `move` en face de la closure pour donner l'appartenance de toutes les variables qu'elle utilise à la fonction.

```rusti
fn main() {
	let y = vec![1, 2, 3];

	// Les {} sont facultatif dans la closure quand il n'y a qu'une seule instruction comme ici
	// L'appartenance de y passe à notre closure
	let addition = move |x| x + y[0] + y[1] + y[2];

	// On ne peut donc plus utiliser y
	println!("Cette commande génère une erreur, commentez là pour supprimer l'erreur: {}", y);

	// Utilisons notre closure
	println!("1 + 6 = {}", addition(1));
}
```

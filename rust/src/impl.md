# Structures, implémentations et traits
## Structure
Un `struct` ou *structure* est un type de donnée personnalisé qui permet de rassembler plusieurs propriétés. (C'est un peu comme un "objet" dans la POO)

```rust
// Création d'une première structure "Chapitre"
struct Chapitre {
  titre: String,
  numero: u64,
  contenu: String
}

// Création d'une deuxième structure "Livre" qui contient entreautre, une liste de "Chapitre"
struct Livre {
  titre: String,
  auteur: String,
  contenu: Vec<Chapitre>
}

fn main() {
    // Création d'un chapitre et d'un livre
    let chapitre = Chapitre {
        titre: "Mon super petit chapitre".to_string(),
        numero: 1,
        contenu: "voilà, le chapitre est fini...".to_string()
    };
    
    let livre = Livre {
        titre: "Livre court".to_string(),
        auteur: "Anonyme".to_string(),
        contenu: vec![chapitre]
    };

    println!("Mon livre est '{}' et le premier chapitre est '{}'", livre.titre, livre.contenu[0].titre);
}
```

## Implémentation et trait
Un *trait* sert à définir un comportement partagé de manière abstraite par plusieurs types. 

Une *implémentation* permet de grouper des méthodes (fonctions) pour un type donné. Par exemple sur un *struct*, il va y avoir à la fois les propriétés du *struct* et les méthodes de son implémenation.

Ainsi une implémentation peut être utilisée pour implémenter un *trait* sur un type.

```rust
// Le trait "Resumable" dit que tout type implémentant ce trait doit avoir la méthode "resumer"
pub trait Resumable {
    // Le &self est important car sinon il devient propriétaire de lui même et on ne peut donc plus l'utiliser
    fn resumer(&self) -> String;
    // On utilise &mut pour pouvoir faire en sorte que l'objet puisse modifier une instance de lui même
    fn anonymiser(&mut self);
}

// La structure ArticleDePresse contient 4 propriétés de type String
pub struct ArticleDePresse {
    pub titre: String,
    pub lieu: String,
    pub auteur: String,
    pub contenu: String,
}

// Implémente le trait Resumable pour le type ArticleDePresse
impl Resumable for ArticleDePresse {
    // Implémente la méthode "résumer"
    // Le &self est important car si on met juste 'self' il devient propriétaire de lui même et on ne peut donc plus l'utiliser
    fn resumer(&self) -> String {
        format!("{}, par {} ({})", self.titre, self.auteur, self.lieu)
    }

    // On utilise &mut pour pouvoir faire en sorte que l'objet puisse modifier une instance de lui même
    fn anonymiser(&mut self) {
        self.auteur = "Anonyme".to_string();
        self.lieu = "Inconnu".to_string();
    }
}

// Crée ArticleDePresse dans la variable "article" et exécute la méthode "resumer"
fn main() {
    // On va utiliser la méthode anonymiser plus tard donc on met "mut" sur notre variable pour pouvoir la modifier
    let mut article = ArticleDePresse {
            titre: "Mon titre".to_string(),
            lieu: "Liège".to_string(),
            auteur: "John Doe".to_string(),
            contenu: "ayayayaya".to_string()
        };

    article.anonymiser();
    println!("{}", article.resumer());
}

```

Mais il est aussi possible de faire la même chose, sans trait. 

```rust
// La structure ArticleDePresse contient 4 propriétés de type String
pub struct ArticleDePresse {
    pub titre: String,
    pub lieu: String,
    pub auteur: String,
    pub contenu: String,
}

// Implémente le type ArticleDePresse
impl ArticleDePresse {
    // Implémente la méthode "résumer"
    // Le &self est important car si on met juste 'self' il devient propriétaire de lui même et on ne peut donc plus l'utiliser
    fn resumer(&self) -> String {
        format!("{}, par {} ({})", self.titre, self.auteur, self.lieu)
    }

    fn anonymiser(&mut self) {
        self.auteur = "Anonyme".to_string();
        self.lieu = "Inconnu".to_string();
    }
}

// Crée ArticleDePresse dans la variable "article" et exécute la méthode "resumer"
fn main() {
    // On va utiliser la méthode anonymiser plus tard donc on met "mut" sur notre variable pour pouvoir la modifier
    let mut article = ArticleDePresse {
            titre: "Mon titre".to_string(),
            lieu: "Liège".to_string(),
            auteur: "John Doe".to_string(),
            contenu: "ayayayaya".to_string()
        };

    article.anonymiser();
    println!("{}", article.resumer());
}
```

## En savoir plus
* [*Le language de programmation Rust* - Définir et instancier des structures](https://jimskapt.github.io/rust-book-fr/ch05-01-defining-structs.html)
* [*Le language de programmation Rust* - Définir des comportements partagés avec les traits](https://jimskapt.github.io/rust-book-fr/ch10-02-traits.html)
* [*Rust By Example* - Structures](https://doc.rust-lang.org/stable/rust-by-example/custom_types/structs.html)
* [*Rust By Example* - Traits](https://doc.rust-lang.org/stable/rust-by-example/trait.html)
* [*Rust By Example* - Implementation](https://doc.rust-lang.org/stable/rust-by-example/generics/impl.html)
# Struct & Enums
Un `struct` ou *structure* est un type de donnée personnalisé qui permet de rassembler plusieurs propriétés. (C'est comme un "objet" dans la POO)

```rust
// Création d'une première structure "Chapitre"
struct Chapitre {
  titre: String,
  numero: u64,
  contenu: String
}

// Création d'une deuxième structure "Livre" qui contient entreautre, une liste de "Chapitre"
struct Livre {
  titre: String,
  auteur: String,
  contenu: Vec<Chapitre>
}

fn main() {
    // Création d'un chapitre et d'un livre
    let chapitre = Chapitre {
        titre: "Mon super petit chapitre".to_string(),
        numero: 1,
        contenu: "voilà, le chapitre est fini...".to_string()
    };
    
    let livre = Livre {
        titre: "Livre court".to_string(),
        auteur: "Anonyme".to_string(),
        contenu: vec![chapitre]
    };

    println!("Mon livre est '{}' et le premier chapitre est '{}'", livre.titre, livre.contenu[0].titre);
}
```

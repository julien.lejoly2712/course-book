# Summary

- [🐧 Linux]()
    - [🖥️ Installation/achat d'un VPS]()
    - [🪄 Commandes de base]()
    - [🔒 Sécurité de base]()
- [🧪 Git](git.md)
    - [🍳 Quelques recettes](git-1.md)
    - [👩🏾‍💻 Publier et collaborer](git-2.md)
    - [👽 Gérer des univers parallèles](git-3.md)
    - [⏳ Retourner dans le temps]()
- [🚀 CI/CD avec gitlab](gitlab.md)
    - [📄 Exemple avec Pages](gitlab-pages.md)
    - [🔑 Fonctionnement avec SSH](ssh-ci.md)
    - [🛰 Utiliser son propre Runner](runner.md)
    - [🤖 Automatisation de Kubernetes et Docker]()
- [📦 Containers](containers.md)
    - [🔧 Utilisation]()
    - [📤 Dockerizer et publier](dockerize.md)
- [Kubernetes]()
    - [Installation]()
    - [Configuration]()

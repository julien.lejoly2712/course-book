# 📦 Conteneurs (ou pod)
Docker est un logiciel qui est pas mal utilisé pour installer des services sur un serveur.

L'avantage c'est que Docker va virtualiser les services dans des genres de "mini-machine virtuelles". Ce qui permet :

* D'avoir un environement isolé et constant. Permetant de eployer quelque chose quel que soit l'état de la machine hôte. 
* Deployer des services rapidement et facilement
* Permet une plus grande extensibilité
* Plus sécurisé grace à l'isolation
* Il est aussi possible de revenir dans des états précédents d'un pod

C'est nottament ce qui est utilisé quand on a utilisé Gitlab-CI pour l'automatisation. 

Et c'est ce que l'on va de nouveau utiliser quand on va utiliser Kubernetes pour avoir une plus grande extensibilité.

## Installation
Il y a différent logiciels qui permettent de faire ceci, mais dans ce cas on va utiliser `podman`, plus rapide et plus sur que Docker.

Mais podman a la même syntaxe que Docker donc pas de problème à ce niveau là.

* [Site de Podman et instructions d'installation](https://podman.io/getting-started/installation)

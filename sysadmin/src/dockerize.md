# 🐳 Dockerizer et publier
Dans ce petit exemple j'ai voulu dockerizé une image de Debian avec `openjdk-18-jdk`. 

## Tester des choses dans un pod
On sais que l'on veut partir d'une image debian. Mais on ne sais peut'être pas les instructions exacte pour y installer openjdk-18-jdk. Donc on peut créer un pod Debian et l'utiliser comme environement de test.

```bash
podman run --name debian-test -it docker.io/library/debian
```

Maintenant, on a une distribution Debian pour y faire nos tests.

## Créer le dockerfile et le publier
Pour cela on va d'abord créer un dossier vide pour notre projet

```bash
mkdir docker-openjdk
cd docker-openjdk
```

Maintenant on peut créer un fichier `Dockerfile` et y ajouter ceci: 

```dockerfile
# On part d'une image de debian
FROM debian

# On ajoute le répertoire SID de debian, met a jour et installe le paquet openjdk-18-jdk
RUN echo "deb http://deb.debian.org/debian sid main contrib non-free" >> /etc/apt/sources.list
RUN apt-get update
# On fait également attention a ce que aucune commande ne nécessite une intéraction de l'utilisateur avec "-y"
RUN apt-get install -y openjdk-18-jdk

# On indique la commande principale qui va être lançée quand on crée un pod avec l'image
CMD [ "/bin/bash" ]
```

Une fois cela fait on peut build l'image, on va mettre comme nom `docker.io/<username>/debian-openjdk-18` car c'est l'addresse vers laquelle on va publier l'image plus tard.

```bash
sudo podman build . -t docker.io/<username>/debian-openjdk-18
```

Maintenant, on peut aller sur [Docker Hub](https://hub.docker.com), créer un nouveau repo. Puis ensuite on va se connecter via podman et publier le tout

```bash
sudo podman login docker.io
sudo podman push docker.io/<username>/debian-openjdk-18
```

Maintenant l'image a bien été créée à partir de nos instructions et a été publiée sur le registery de Docker Hub. On peut maintenant tester si l'image fonctionne correctement en lançant un nouveau pod.

```bash
podman run --name debian-openjdk-18-test -it docker.io/<username>/debian-openjdk-18
```

Nous pouvons maintenant par exemple l'utiliser dans un GitLab-CI par exemple.

```yaml
image: <username>/debian-openjdk-18
```

# 🚀 Automatisation avec GitLab-CI
Dans le chapitre précédent nous avons vu comment utiliser Git pour gérer l'historique de fichiers, s'organiser, collaborer avec d'autres développeurs, etc.

Maintenant nous allons voir un autre aspect très intéressant de Git qui est, l'automatisation, ou *Continuous Integration / Continuous Deployment* qui permet qu'a chaque push sur une branche donnée, un script s'exécute pour tester, build et deployer le projet automatiquement.

Cela est donc un très grand gain de temps et ce n'est pas très compliqué. Pour cela il faut déjà maitriser Git et se créer un compte sur Gitlab.com.


# 🛰 Héberger son propre GitLab-runner
Toutes les instances n'ont pas des runner partagés. Donc dans certains cas vous allez peut'être avoir besoin de créer votre runner vous-même.

Cela peut aussi être fait quand on veut que le runner tourne dans un certain réseau par exemple (pour avoir accès à certains serveurs, etc)

## Installation 
Tout d'abord il faut installer docker et gitlab-runner. Vous pouvez trouver les instructions sur leurs sites respectifs. Mais voici les instructions pour Arch Linux

```bash
yay -S gitlab-runner docker
```

Ensuite on peut enregister un nouveau runner

```bash
sudo gitlab-runner register
```

Ici, il faut aller dans les paramètres du projet sur GitLab, dans CI/CD, dans la section "Runners" et voir dans les instructions pour les runners specifiques. Donc:

* GitLab instance URL = voir dans les paramètres
* Registration token  = voir dans les paramètres
* Description = N/A
* Tags = N/A
* Maintenance note = N/A
* Executor = docker

Une fois créé on peut le lancer en faisant 

```bash
sudo gitlab-runner start
```

On peut aussi utiliser la commande suivante pour avoir plus de détails

```bash
sudo gitlab-runner --debug run
```

Et voilà, c'est configuré. Si on installe cela sur un serveur, le runner pourra être disponible 24h/24


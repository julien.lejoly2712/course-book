#!/bin/bash
cd ~/course-book
ls -d */ | grep -v "theme" | xargs -I{} cp -rv theme/ {}
ls -d */ | grep -v "theme" | xargs -I{} mdbook build {}
rsync -rv * debian@snowcode.ovh:/var/www/snowcode.ovh/notes/ 
